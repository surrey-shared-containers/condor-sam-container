# FROM nvidia/cuda:11.2.2-cudnn8-devel-ubuntu20.04
FROM nvidia/driver:460.73.01-ubuntu20.04

ARG DEBIAN_FRONTEND=noninteractive

# Add NVIDIA GPG key to prevent GPG error
# RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub

# RUN apt-key del 7fa2af80

# RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-keyring_1.0-1_all.deb && \
#     dpkg -i cuda-keyring_1.0-1_all.deb 

RUN apt-key del 7fa2af80
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/3bf863cc.pub
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu2004/x86_64/7fa2af80.pub

# RUN rm /etc/apt/sources.list.d/cuda.list
# RUN rm /etc/apt/sources.list.d/nvidia-ml.list

RUN apt-get -y update && apt-get install -y \
    build-essential \
    cmake \
    ninja-build \
    git \
    wget \
    rsync \
    ffmpeg \
    htop \
    nano \
    libatlas-base-dev \
    libboost-all-dev \
    libeigen3-dev \
    libhdf5-serial-dev \
    libleveldb-dev \
    liblmdb-dev \
    libopenblas-dev \
    libopenblas-base \
    libsm6 \
    libxext6 \
    libxrender-dev \
    glibc-source && \
    apt-get autoremove -y && \
    apt-get autoclean -y && \
    apt-get clean -y  && \
    rm -rf /var/lib/apt/lists/*

ENV WRKSPCE="/workspace"

RUN wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b -p $WRKSPCE/miniconda3 \
    && rm -f Miniconda3-latest-Linux-x86_64.sh

ENV PATH="$WRKSPCE/miniconda3/bin:${PATH}"

RUN conda update -c defaults conda

# Create the environment and install all the requirements
RUN conda create -n segment_anything_env python=3.8 -y && \
    conda init bash && \
    echo "conda activate segment_anything_env" >> ~/.bashrc

# Make RUN commands use the new environment:
SHELL ["conda", "run", "-n", "segment_anything_env", "/bin/bash", "-c"]

# Install the requirements
RUN conda create -n segment_anything_env python=3.8 -y && \
    conda run -n segment_anything_env pip install setuptools==60.9.3 && \
    git clone https://github.com/facebookresearch/segment-anything.git && \
    conda run -n segment_anything_env /bin/bash -c "cd segment-anything && pip install -e ." && \
    conda run -n segment_anything_env pip install opencv-python pycocotools matplotlib onnxruntime onnx && \
    conda run -n segment_anything_env conda install pytorch==1.7.1 torchvision==0.8.2 torchaudio==0.7.2 cudatoolkit=11.0 -c pytorch

#conda install pytorch torchvision torchaudio cudatoolkit=11.2 -c pytorch -c nvidia
    #conda install pytorch==1.7.1 torchvision==0.8.2 torchaudio==0.7.2 cudatoolkit=11.0 -c pytorch

# Set the default command to bash to activate conda environment on run
CMD ["/bin/bash"]

